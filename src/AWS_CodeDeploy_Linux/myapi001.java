package AWS_CodeDeploy_Linux;

import java.io.IOException;

import javax.ws.rs.GET;

import javax.ws.rs.Path;

import javax.ws.rs.QueryParam;

import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;

@Path("/myapi001")
public class myapi001 {

	JsonObject jsonObject = new JsonObject();

	@GET
	public Response PostFromPath(

			@QueryParam("searchtype") String searchtype

	) throws IOException {

		jsonObject.addProperty("testVar01", "value0004");

		return Response

				.status(200)

				.entity(jsonObject.toString())

				.header("charser", "utf-8")

				.header("Access-Control-Allow-Origin", "*")

				.header("Access-Control-Allow-Methods",

						"POST, GET, PUT, UPDATE, OPTIONS")

				.header("Access-Control-Allow-Headers",

						"Content-Type, Accept, X-Requested-With")
				.build();

	}

}